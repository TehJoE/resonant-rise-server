#!/usr/bin/env bash

pushd /vagrant/minecraft/server

if ! screen -r -S minecraft -X stuff $'\nsave-all\nsave-off\n';
then
    echo "Screen is attached. Not taking world backup!"
    exit 1
fi

CMD='inotifywait -m -r world | grep -v ACCESS'

# Ensure minecraft has finished writing its files out:
waitsilence -timeout 5s -command "$CMD"

now=$(date +"%Y-%m-%d %H:%M:%S")

echo ">>> [$now] Beginning backup" >> backup.log

git commit -a -m "[$now] Auto-backup commit" >> backup.log
git push origin master >> backup.log

screen -S minecraft -X stuff $'save-on\nsay World backup completed.\n'

popd