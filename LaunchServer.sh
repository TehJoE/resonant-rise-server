#!/usr/bin/env bash

set -e

let memory=$(cat /proc/meminfo | grep -i memtotal | sed -r 's/MemTotal: *(.*) kB/\1/')
memory=$(echo "$memory/1024" | bc)
let half=$(echo "$memory/2" | bc)
let quarter=$(echo "$half/2" | bc)
let threeQuarter=$(echo "$quarter*3" | bc | cut -d. -f1)

minecraft_java_options="-Xmn"$quarter"M -Xss4M -Xms"$threeQuarter"M -Xmx"$threeQuarter"M -XX:+UseConcMarkSweepGC -XX:PermSize=256M -XX:+AggressiveOpts -XX:+UseFastAccessorMethods \
 -XX:+UseStringCache -XX:+OptimizeStringConcat -XX:+UseCompressedStrings -XX:+UseBiasedLocking -Xincgc -XX:MaxGCPauseMillis=10 -XX:SoftRefLRUPolicyMSPerMB=10000 \
 -XX:+CMSParallelRemarkEnabled -XX:ParallelGCThreads=10 -Djava.net.preferIPv4Stack=true"

java $minecraft_java_options -jar forge-1.7.10-10.13.4.1448-1.7.10-universal.jar nogui 2>&1 | tee -a server.log